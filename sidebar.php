<div id="sidebar">

	<section class="news">

						<h3>最新記事一覧</h3>
            <div class="h_line"></div>
	<ul class="cf">
	<?php
		$wp_query = new WP_Query();
		$param = array(
			'posts_per_page' => '10', //表示件数。-1なら全件表示
			'post_status' => 'publish',
			'orderby' => 'date', //ID順に並び替え
			'order' => 'DESC'
		);
		$wp_query->query($param);?>
	<?php if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post(); ?>
		
			<li>
	<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">

<?php $title= mb_substr($post->post_title,0,30); echo $title;?>
		</a></li>
	
	<?php endwhile; ?>
	<?php endif; ?></ul>
	<?php wp_reset_query(); ?>
	</section>
</div>
<!-- sidebar -->