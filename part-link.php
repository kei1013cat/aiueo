<section class="pd">
    <div class="wrapper660">
        <h3 class="headline01 enter-top">リンク</h3>


        <dl class="enter-bottom">
            <dt>札幌市児童相談所</dt>
            <dd><a href="http://www.city.sapporo.jp/kodomo/jisou/jidousoudansho.html" target="_blank">http://www.city.sapporo.jp/kodomo/jisou/jidousoudansho.html</a></dd>
        </dl>
        <dl class="enter-bottom">
            <dt>北海道　保健福祉部福祉局障害者保健福祉課</dt>
            <dd><a href="http://www.pref.hokkaido.lg.jp/hf/shf/" target="_blank">http://www.pref.hokkaido.lg.jp/hf/shf/</a></dd>
        </dl>
        <dl class="enter-bottom">
            <dt>各区健康・子ども課子育て支援係<br>
                各区保健センター（健康・子ども課）</dt>
            <dd><a href="http://www.city.sapporo.jp/kodomo/kosodate/Q1_07.html" target="_blank">http://www.city.sapporo.jp/kodomo/kosodate/Q1_07.html</a></dd>
        </dl>
        <dl class="enter-bottom">
            <dt>子育て支援ポータルサイト 子育て北海道</dt>
            <dd><a href="http://www.kosodate-hokkaido.com/index.html" target="_blank">http://www.kosodate-hokkaido.com/index.html</a></dd>
        </dl>
        <dl class="enter-bottom">
            <dt>げんきサーチ<br>
                札幌市障害福祉事業所等の空き情報</dt>
            <dd><a href="http://www.sapporo-akijoho.jp/" target="_blank">http://www.sapporo-akijoho.jp/</a></dd>
        </dl>
        <dl class="enter-bottom">
            <dt>ぐりんくねっと 北海道版</dt>
            <dd><a href="http://www.g-linknet.co.jp/gli/dayservice/" target="_blank">http://www.g-linknet.co.jp/gli/dayservice/</a></dd>
        </dl>
        <dl class="enter-bottom">
            <dt>嘱託契約医療機関｜篠路こどもクリニック</dt>
            <dd><a href="http://shinoro-kodomo.jp/xiao_lukodomokurinikku/goaisatsu.html" target="_blank">http://shinoro-kodomo.jp/xiao_lukodomokurinikku/goaisatsu.html</a>
            </dd>
        </dl>

        <dl class="enter-bottom">
            <dt>いちにいさん児童発達支援・放課後等デイサービス</dt>
            <dd><a href="http://123codomo.com/" target="_blank">http://123codomo.com/</a> </dd>
        </dl>
        <dl class="enter-bottom">
            <dt>ココロ児童発達支援・放課後等デイサービス</dt>
            <dd><a href="http://wellnesscare.jp/" target="_blank">http://wellnesscare.jp/</a> </dd>
        </dl>
        <dl class="enter-bottom">
            <dt>ABC児童発達支援・放課後等デイサービス</dt>
            <dd><a href="http://abc-codomo.com/" target="_blank">http://abc-codomo.com/</a> </dd>
        </dl>
        <dl class="enter-bottom">
            <dt>HOP児童発達支援・放課後等デイサービス</dt>
            <dd><a href="http://hop-codomo.com/" target="_blank">http://hop-codomo.com</a> </dd>
        </dl>
        <dl class="enter-bottom">
            <dt>ぐるり児童発達支援・放課後等デイサービス</dt>
            <dd><a href="http://gururi-codomo.com/" target="_blank">http://gururi-codomo.com</a> </dd>
        </dl>
        <dl class="enter-bottom">
            <dt>アポロ児童発達支援・放課後等デイサービス</dt>
            <dd><a href="http://apollo-codomo.com/" target="_blank">http://apollo-codomo.com/</a> </dd>
        </dl>
        <dl class="enter-bottom">
            <dt>トーマス児童発達支援・放課後等デイサービス</dt>
            <dd><a href="http://thomas-codomo.com/" target="_blank">http://thomas-codomo.com/</a> </dd>
        </dl>
        <dl class="enter-bottom">
            <dt>ドレミ児童発達支援・放課後等デイサービス</dt>
            <dd><a href="http://doremi-codomo.com/" target="_blank">http://thomas-codomo.com/</a> </dd>
        </dl>
        <dl class="enter-bottom">
            <dt>社会福祉法人　札親会<br>
                札北荘　ゆめくる　（通所/生活介護）</dt>
            <dd><a href="http://satsuoyakai.or.jp/%E6%9C%AD%E5%8C%97%E8%8D%98%E9%80%9A%E6%89%80-2/" target="_blank">http://satsuoyakai.or.jp/</a> </dd>
        </dl>
        <dl class="enter-bottom">
            <dt>社会福祉法人　渓仁会<br>
                札幌市障がい者相談支援事業所<br>
                相談室こころ ていね</dt>
            <dd><a href="http://www.keijinkai.com/salanet/news/130304.html" target="_blank">http://www.keijinkai.com/salanet/news/130304.html</a> </dd>
        </dl>

    </div>
    <!-- wrapper660 -->
</section>
