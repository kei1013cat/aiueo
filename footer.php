<section class="shopinfo">
    <div class="wrapper660 enter-bottom">
        <h2><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/footer_h.png" alt="札幌市東区　あいうえお児童発達支援・放課後等デイサービス（児童デイサービス）"></a></h2>

        <div class="box cf">
            <p class="tel"><a href="tel:011-792-1182"><img src="<?php bloginfo('template_url'); ?>/images/footer_tel.png" alt="電話番号「011-792-1182」"></a>
                <br><span class="time">受付時間 ： (月～土) 9:00 ~ 18:00</span></p>
            <p class="btn"><a href="<?php bloginfo('url'); ?>/contact/"><span>メールでのお問い合わせ</span></a></p>
        </div>
        <!-- box -->
        <address>
            〒065-0042 <span class="pc">　</span><br class="sp">札幌市東区本町2条2丁目5番32号</address>
    </div>
    <!-- wrapper -->
    <div id="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1588.9589024513145!2d141.38472115681748!3d43.08005710548298!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f0b2947d75d2ea7%3A0x6bd91bce31f46252!2z44CSMDY1LTAwNDIg5YyX5rW36YGT5pyt5bmM5biC5p2x5Yy65pys55S677yS5p2h77yS5LiB55uu77yV4oiS77yT77yS!5e0!3m2!1sja!2sjp!4v1553267727480" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!-- map -->



</section>
<footer class="global">
    <p class="pagetop"><a href="#outer"><img src="<?php bloginfo('template_url'); ?>/images/pagetop.png" alt="pagetop"></a></p>


    <nav>
        <div class="wrapper">
            <ul class="cf">
                <li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
                <li><a href="<?php bloginfo('url'); ?>/about/">施設のご案内</a></li>
                <li><a href="<?php bloginfo('url'); ?>/flow/">ご利用の流れ</a></li>
                <li><a href="<?php bloginfo('url'); ?>/faq/">よくあるご質問</a></li>
                <li><a href="<?php bloginfo('url'); ?>/introduction/">事業案内</a></li>
                <li><a href="<?php bloginfo('url'); ?>/company/">会社概要</a></li>
                <li><a href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>
            </ul>
            <ul class="cf">
              <!--  <li><a href="<?php bloginfo('template_url'); ?>/images/hyouka2019_.pdf" target="_blank">自己評価表</a></li>-->
                <li><a href="<?php bloginfo('url'); ?>/link/">リンク</a></li>
                <li><a href="<?php bloginfo('url'); ?>/site/">サイトマップ</a></li>
                <li><a href="<?php bloginfo('url'); ?>/privacy/">プライバシーポリシー</a></li>
            </ul>
        </div>
        <!-- wrapper -->
    </nav>
    <ul class="bnr cf">
        <li><a href="http://shinoro-kodomo.jp/xiao_lukodomokurinikku/goaisatsu.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/footer_bnr_clinic.jpg" alt="篠路こどもクリニック" class="alpha80"></a></li>
        <li><a href="<?php bloginfo('url'); ?>/staffblog/"><img src="<?php bloginfo('template_url'); ?>/images/footer_bnr_fb.jpg" alt="あいうえおスタッフブログ" class="alpha80"></a></li>
    </ul>

    <p class="copy">copyright &copy; aiueo-codomo.com All Rights Reserved.</p>
</footer>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/lightbox.js"></script>
</main>
</div>
<!-- outer -->
</body>

</html>
