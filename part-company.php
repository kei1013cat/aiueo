<section class="pd info">
    <div class="wrapper_inner">
        <h3 class="headline01 enter-top">会社概要</h3>
        <section>


<table border="0" class="style1">
				<tbody><tr>
					<th scope="row">名称</th>
					<td>児童発達支援・放課後等デイサービス　あいうえお</td>
				</tr>
                
				<tr>
					<th scope="row">事業所番号</th>
					<td>0150301067</td>
				</tr>
				<tr>
					<th scope="row">所在地</th>
					<td>〒065-0042<br />札幌市東区本町2条2丁目5番32号</td>
				</tr>
				<tr>
					<th scope="row">連絡先</th>
					<td>TEL:011-792-1182<br /> FAX:011-792-1184</td>
				</tr>
				<tr>
					<th scope="row">商号</th>
					<td style="vertical-align:middle;"><img src="<?php bloginfo('template_url'); ?>/images/conpany_com_name.jpg" style="position:relative;top:-5px;"></td>
				</tr>
				<tr>
					<th scope="row">設立</th>
					<td>平成11年10月20日</td>
				</tr>
				<tr>
					<th scope="row">代表者</th>
					<td style="vertical-align:middle;"><img src="<?php bloginfo('template_url'); ?>/images/conpany_ceo_name.jpg" style="position:relative;top:-7px;"></td>
				</tr>
				<tr>
					<th scope="row">目的</th>
					<td>1.　児童福祉法に基づく障害児通所支援事業<br /> 2.　児童福祉法に基づく児童発達支援及び放課後等デイサービス事業<br /> 3.　障害者自立支援法に基づく障害福祉サービス事業<br /> 4.　障害者自立支援法に基づく相談支援事業<br /> 5.　障害者自立支援法に基づく地域生活支援事業<br /> 6.　保育所の設置運営<br /> 7.　放課後児童健全育成事業<br /> 8.　子育て支援事業 </td>
				</tr>
                <tr>
          <th scope="row">顧問</th>
          <td>■法律顧問<br> 　磯田健人　法律事務所<br> 　弁護士　磯田 健人<br> <br> ■税務顧問<br> 　佐藤利則　税理士事務所<br> 　所長　佐藤 直規<br> <br> ■社会保険労務顧問<br> 　ベンチャーパートナーズ社会保険労務士法人<br> 　社会保険労務士　山本 祐一郎<br> <br> ■行政法務顧問<br> 　シティ行政法務事務所<br> 　行政書士　藤永 誠一郎</td>
        </tr>
			</tbody></table>

            

    </div>
    <!-- wrapper_inner -->
</section>
