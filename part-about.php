<section class="about_top pd">
    <div class="wrapper">
        <h3 class="headline01 enter-top">施設のご案内</h3>
        <h3 class="enter-bottom"><img src="<?php bloginfo('template_url'); ?>/images/index_about_h.png" alt="あいうえお児童デイサービスの特徴"></h3>
        <p class="enter-bottom tac">基本的な日常生活の動作や集団生活など、<br class="pc">コミュニケーション能力・適応力が向上するサポートをしていきます。<br>沢山褒めて「意欲・楽しいJにつながる支援を大切にします。<br class="pc">保育士・幼稚園教諭等の資格保持者のベテランスタッフがサポートします。<br class="pc">「あいうえお」では、発達の順番を決して飛び越えることなく<br class="pc">一人ひとりに適した療育を行つていきます。
        </p>
        <div class="photo cf">
			<p class="col1"><img src="<?php bloginfo('template_url'); ?>/images/about_photo1.jpg"></p>
			<p class="col2"><img src="<?php bloginfo('template_url'); ?>/images/about_photo2.jpg"></p>
		</div>

        <div id="about_hop">
        </div>
        <!-- about_hop -->
    </div>
    <!-- wrapper_inner -->
</section>
<section class="transport pd bg_beige ">
    <div class="wrapper_inner">
        <h3 class="headline01 enter-top">あいうえおの送迎について</h3>
        <p class="enter-bottom">あいうえお児童デイサービスでは、毎日お子さまの送迎を行っております。<br class="pc">施設とご自宅との送迎、施設から学校へのお迎えもいたします。<br class="pc">送迎範囲は北区、東区、白石区、中央区も相談に応じます。<br class="pc">出来る範囲で送迎については対応をさせて頂いております。<br class="pc">詳しくは、ご見学時やお電話等にて、お問い合わせ下さい。</p>
        <!--
        <div class="box enter-bottom">
            受付時間：(月～土) 9：00～18：00 <br>
            TEL：011-792-1182<br>
            E-mail：<a href="mailto:info@aiueo-codomo.com">info@aiueo-codomo.com</a>
        </div>
        -->
        <!-- box -->

    </div>
    <!-- wrapper_inner -->
</section>
