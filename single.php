<?php get_header(); ?>

<div id="contents">
	<?php include (TEMPLATEPATH . '/part-title.php'); ?>
	

	<section id="single" class="pd news_entry cf">
					<h3 class="headline01">スタッフブログ</h3>
            <div class="h_line"></div>
		<div class="wrapper cf">
			<div class="left_contents">
				
				<?php if ( have_posts() ) :?>
				<?php while ( have_posts() ) : the_post(); ?>
				<article <?php post_class(); ?>>
					<div class="entry-header">
						
						<p class="date"><span class="date"><time class="entry-date" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate="<?php the_time( 'Y-m-d' ); ?>">
								<?php the_time( 'Y.m.d'  ); ?></time></span></p>
						<h3 class="entry-title">
							<?php the_title(); ?>
						</h3>
						
					</div>
					<section class="entry-content">
						<?php the_content(); ?>
					</section>
					<ul class="page_link cf">
<li class="prev"><?php previous_post_link('%link', '« 前の記事へ', TRUE); ?></li>
<li class="next"><?php next_post_link('%link', '次の記事へ »', TRUE); ?></li>
					</ul>
				</article>
				<?php endwhile; endif; ?>
				<?php wp_reset_query(); ?>
			</div>
			<!-- left_contents -->
			
			<?php get_sidebar(); ?>
		</div>
		<!-- wrapper --> 
	</section>
</div>
<!-- contents -->

<?php get_footer(); ?>
