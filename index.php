<?php get_header(); ?>

<div id="main_img">
    <div class="slider-pro" id="slider1">
        <div class="sp-slides">
            <div class="sp-slide"> <img class="sp-image" src="<?php bloginfo('template_url'); ?>/images/slide01<?php mobile_img(); ?>.jpg?v=20180306" /> </div>
            <div class="sp-slide"> <img class="sp-image" src="<?php bloginfo('template_url'); ?>/images/slide02<?php mobile_img(); ?>.jpg?v=20180306" /> </div>
            <?php if(is_pc()):?><div class="sp-slide"> <img class="sp-image" src="<?php bloginfo('template_url'); ?>/images/slide03.jpg?v=20180306" /> </div><?php endif; ?>
        </div>
    </div>
</div>
<!-- main_img -->
<div id="contents">

    <section class="about">
        <div class="wrapper">
            <div class="grid1 enter-left">
                <div class="photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/about_photo1.jpg" alt="児童発達支援・放課後等デイサービス　あいうえお">
                </div>
                <!-- photo -->
                <div class="text">
                    <h2>沢山褒めて「意欲・楽しい」に<br>つながる支援を大切に</h2>
                    <p>「言葉が遅れている、落ち着きがない、こだわりが強い、子どもの対応がわからない」など、お子さまを育てるうえでママが困った時に、一緒に協力して子育てをしていくのが児童デイサービスです。<br>
                        心身の障がいという個性を持つお子さまが、就学や社会に出る時に少しでも不安を軽減して自立できるよう療育いたします。お子さまに気になることがありましたらどんな些細な事でもお気軽にご相談ください。一緒に解決していきましょう。
                    </p>
                </div>
                <!-- text -->
            </div>
            <!-- grid -->


            <?php if(is_pc()): ?>

            <div class="grid2 enter-right">
                <div class="text">
                    <h2>「コミュニケーション能力・適応力」が<br>向上するサポートをしています</h2>
                    <p>「うちの子、もうすぐ一年生なのにひらがなが書けない！」このようなことでお困りの方はいませんか？<br>
                        ひらがなは、〇（マル）△（サンカク）□（シカク）を描くことが出来ないと書くことが難しいといわれます。<br>
                        「あいうえお」では発達の順番を決して飛び越えることなく一人ひとりに適した療育を行っていきます。
                    </p>
                </div>
                <!-- text -->
                <div class="photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/about_photo2.jpg" alt="児童発達支援・放課後等デイサービス　あいうえお">
                </div>
                <!-- photo -->
            </div>
            <!-- grid -->
            <?php else: ?>
            <div class="photo">
                <img src="<?php bloginfo('template_url'); ?>/images/about_photo2.jpg" alt="児童発達支援・放課後等デイサービス　あいうえお">
            </div>
            <!-- photo -->
            <div class="text">
                <h2>「コミュニケーション能力・適応力」が<br class="sp">向上するサポートをしています</h2>
                <p>「うちの子、もうすぐ一年生なのにひらがなが書けない！」このようなことでお困りの方はいませんか？<br>
                    ひらがなは、〇（マル）△（サンカク）□（シカク）を描くことが出来ないと書くことが難しいといわれます。<br>
                    「あいうえお」では発達の順番を決して飛び越えることなく一人ひとりに適した療育を行っていきます。
                </p>
            </div>
            <!-- text -->
        </div>
        <!-- grid -->
        <?php endif; ?>

</div><!-- wrapper -->


</section>

<section class="kyusyoku">
    <img class="enter-bottom" src="<?php bloginfo('template_url'); ?>/images/kyusyoku<?php mobile_img(); ?>.jpg" alt="給食あり 栄養士が作成した献立メニュー">
</section>

<section class="sougei">
    <img class="grow3" src="<?php bloginfo('template_url'); ?>/images/sougei.png" alt="送迎のご案内">

    <p class="enter-bottom">「あいうえお」では、毎日のお子さまの送迎を行っております。<br>
        北区・東区・白石区・中央区にお住まいの方はご相談ください。
</section>

<section class="pd day">
    <div class="wrapper_inner">
        <h3 class="enter-top1">あいうえおの一日と年間行事</h3>

        <div class="inner40">


            <p class="sche"><img class="fead1" src="<?php bloginfo('template_url'); ?>/images/event_day_info1.jpg" alt="一日の過ごし方"><img class="fead2" src="<?php bloginfo('template_url'); ?>/images/event_day_info2.jpg" alt="年間行事予定"></p>
        </div>
        <!-- inner40 -->
    </div><!-- wrapper_inner -->
</section>




<?php
    $wp_query = new WP_Query();
    $param = array(
        'posts_per_page' => '5', //表示件数。-1なら全件表示
        'post_status' => 'publish',
        'orderby' => 'date', //ID順に並び替え
        'order' => 'DESC'
    );
    $wp_query->query($param);?>
<?php if($wp_query->have_posts()):?>
<section class="blog bg_color1">
    <div class="wrapper660 cf">
        <h3 class="headline01 enter-top">スタッフブログ</h3>
        <div class="lay_postlist2 enter-bottom">
            <?php while($wp_query->have_posts()) :?>
            <?php $wp_query->the_post(); ?>

            <dl class="cf">
                <dt><?php the_time('Y.m.d'); ?></dt>
                <dd><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php if(mb_strlen($post->post_title)>27) { $title= mb_substr($post->post_title,0,27) ; echo $title. '...' ;
} else {echo $post->post_title;}?></a></dd>
            </dl>

            <?php endwhile; ?>


        </div><!-- lay_postlist2 -->
    </div>
    <!-- wrapper -->
</section>
<?php endif; ?>
<?php wp_reset_query(); ?>






<?php include (TEMPLATEPATH . '/part-other_link.php'); ?>


</div>
<!-- contents -->
<?php get_footer(); ?>
