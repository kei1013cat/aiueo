<section class="pd">
    <div class="wrapper660">
        <h3 class="headline01 enter-top">ご相談・お問い合わせ</h3>
        <div class="h_line"></div>

        <div class="inner40 enter-bottom">
            ご不明な点がございましたら、お気軽にお問い合わせ・ご相談ください。<br />
            下記のフォームに必要事項をご記入の上ご送信下さい。メールを確認次第、担当者からご連絡いたします。<br />
            「<em>※</em>」印の箇所は、必ず入力してください。<br>
            <br>
            <form id="mailformpro" action="<?php bloginfo('template_url'); ?>/form/mailformpro/mailformpro.cgi" method="POST">
                <table border="0" class="style04 enter-bottom">
                    <tr>
                        <th class="bgcolor">お名前<em>※</em></th>
                        <td><input type="text" name="お名前" class="textbox" required="required" id="name" size="40" maxlength="40" /></td>
                    </tr>
                    <tr>
                        <th class="bgcolor">フリガナ</th>
                        <td><input type="text" name="フリガナ" class="textbox" id="kana" size="40" maxlength="40" /></td>
                    </tr>
                    <tr>
                        <th class="bgcolor">メールアドレス<em>※</em></th>
                        <td><input type="text" name="email" class="textbox" id="email" required="required" size="50" maxlength="60" /></td>
                    </tr>
                    <tr>
                        <th class="bgcolor">お電話番号</th>
                        <td><input type="text" name="電話番号" class="textbox" id="tel" size="20" maxlength="50" /></td>
                    </tr>
                    <tr>
                        <th class="bgcolor">お問合せ項目</th>
                        <td><label>
                                <input type="radio" name="お問合せ項目" value="見学希望" />
                                &nbsp;見学希望&nbsp;&nbsp;</label>
                            <label>
                                <input type="radio" name="お問合せ項目" value="相談希望" />
                                &nbsp;相談希望&nbsp;&nbsp;</label>
                            <label>
                                <input type="radio" name="お問合せ項目" value="その他" />
                                &nbsp;その他</label></td>
                    </tr>
                    <tr>
                        <th class="bgcolor">お問合せ内容<em>※</em></th>
                        <td><textarea name="お問合せ内容" cols="55" <?php if(is_pc()):?>rows="25" <?php else: ?>rows="10" <?php endif; ?> id="その他" class="textbox"></textarea></td>
                    </tr>
                </table>
                <div id="submitArea">
                    <p class="privacy">当事業所のプライバシーポリシーについては<a href="<?php bloginfo('url'); ?>/privacy/" target="_blank">こちら</a>をご覧ください。</p>
                    <div class="mfp_buttons">
                        <button type="submit">送信する</button>
                    </div>
                    <!-- /#submitArea -->
                </div>
            </form>
            <script type="text/javascript" id="mfpjs" src="<?php bloginfo('template_url'); ?>/form/mailformpro/mailformpro.cgi" charset="UTF-8"></script>
        </div>
        <!-- inner40 -->
    </div>
    <!-- wrapper -->
</section>
