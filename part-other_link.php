<section class="other_link">
    <div class="wrapper cf">
        <ul class="cf">
            <li class="fead1">
                <a href="http://www.wellnesscare.jp/" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/sidebar_bunner_kokoro.jpg" alt="ココロ児童発達支援・放課後等デイサービス" width="270" height="160" class="alpha85">
                </a>
            </li>
            <li class="fead2">
                <a href="http://123codomo.com" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/side_banner_123.jpg" alt="123" class="alpha85">
                </a>
            </li>
            <li class="fead3">
                <a href="http://abc-codomo.com" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/side_banner_abc.jpg" alt="abc" class="alpha85">
                </a>
            </li>
            <li class="fead1">
                <a href="http://thomas-codomo.com/" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/side_banner_thomas.jpg" alt="トーマス児童発達支援・放課後等デイサービス" class="alpha85">
                </a>
            </li>
            <li class="fead2">
                <a href="http://hop-codomo.com" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/side_banner_hop.jpg" alt="HOP児童デイサービス" class="alpha85">
                </a>
            </li>
            <li class="fead3">
                <a href="http://gururi-codomo.com" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/side_banner_gururi.jpg" alt="ぐるり児童デイサービス" class="alpha85">
                </a>
            </li>
            <li class="fead1">
                <a href="http://apollo-codomo.com" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/side_banner_apollo.jpg" alt="アポロ児童デイサービス" class="alpha85">
                </a>
            </li>
            <li class="fead2">
                <a href="http://doremi-codomo.com" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/bnr_doremi.jpg" alt="ドレミ児童デイサービス" class="alpha85">
                </a>
            </li>
        </ul>

    </div>
</section>
