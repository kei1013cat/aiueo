
<section class="pd">
<div class="wrapper_inner">
            <h3 class="headline01">一日の過ごし方</h3>
            <div class="h_line"></div>
			<div class="inner40">
				<p class="pb10 pt10 pl30">月曜日～土曜日 9：00～18：00(日・祝祭日はお休み)</p>
				<ul class="indent pb20">
					<li> ※親御さんのご要望とお子様の状況に合わせて、9時～18時の中からご利用時間を<span class="pc"><br>
						</span>自由にお選び頂けます。（多少の時間の前後は、ご相談お受けいたします。）</li>
					<li> ※通常のご利用と異なる午前授業後、学校行事、学校振替休日、春休み、夏休み、冬休みなど<span class="pc"><br>
						</span>長期休暇にも対応しておりますのでご相談ください。 </li>
				</ul>
				<p class="sche"><img src="<?php bloginfo('template_url'); ?>/images/event_day_info1.jpg" alt="児童発達支援・放課後デイサービスの過ごし方"><img src="<?php bloginfo('template_url'); ?>/images/event_day_info2.jpg" alt="児童発達支援・放課後デイサービスの過ごし方"></p>
			</div>
			<!-- inner40 -->
            </div><!-- wrapper_inner -->
		</section>
    <?php include (TEMPLATEPATH . '/part-line.php'); ?>