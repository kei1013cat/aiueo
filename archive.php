<?php get_header(); ?>

<div id="contents">
	<section class="news_list bg_beige">
	<div class="wrapper cf">
		<div class="left_contents">
			<section>
				<?php if ( have_posts() ) :?>
				<section>
					<h2 class="headline01">NEW POST<span class="icon_news">新着情報</span></h2>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php
	$category = get_the_category();
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>
					<a href="<?php the_permalink() ?>">
					<dl class="cf">
						<dt><?php echo wp_get_attachment_image(get_post_meta($post->ID,'サムネイル',true), 'thumbnail');?> </dt>
						<dd>
							<p class="cat">
								<?php $category = get_the_category(); ?>
								<span class="icon_<?php echo $category[0]->category_nicename; ?>"><?php echo $category[0]->cat_name; ?></p>
							<p class="date"> <span class="date">
								<?php the_time('Y.m.d') ?> <?php echo get_post_time('D'); ?>
								</span><?php /*<span class="name">text by
								<?php the_author_nickname(); ?>
								</span> */?></p>
							<p class="text">
								<?php the_title(); ?>
							</p>
						</dd>
					</dl>
					</a>
					<?php endwhile; ?>
				</section>
				<div class="pagination"> <?php echo bmPageNaviGallery(); // ページネーション出力 ?> </div>
				<!-- pagination -->
				<?php else : ?>
				<p class="tac">記事が見つかりません。</p>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
			</section>
		</div>
		<!-- left_cont -->
		
		<?php get_sidebar(); ?>
	</div>
	<!-- wrapper --> 
	</section>
</div>
<!-- contents -->

<?php get_footer(); ?>
