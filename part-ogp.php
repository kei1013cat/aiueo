<meta property='og:locale' content='ja_JP'>
<meta property='fb:app_id' content='1868715233366740'>
<meta property='og:site_name' content='<?php bloginfo('name'); ?>'>
<?php
if (is_single()){
if(have_posts()): while(have_posts()): the_post();
echo '<meta property="og:title" content="'; the_title(); echo '">';echo "\n";
echo '<meta property="og:description" content="'.mb_substr(get_the_excerpt(), 0, 100).'">';echo "\n";
echo '<meta property="og:url" content="'; the_permalink(); echo '">';echo "\n";
echo '<meta property="og:type" content="article">';echo "\n";
echo '<meta property="article:publisher" content="https://www.facebook.com/%E3%81%AA%E3%81%8B%E3%82%88%E3%81%97%E3%83%87%E3%82%A4%E3%82%B5%E3%83%BC%E3%83%93%E3%82%B9%E3%81%92%E3%82%93%E3%81%8D%E3%81%BE%E3%82%8B-468120060059868/">';echo "\n";
endwhile; endif;
} else {
echo '<meta property="og:title" content="'; bloginfo('name'); echo '">';echo "\n";
echo '<meta property="og:description" content="'; bloginfo('description'); echo '">';echo "\n";
echo '<meta property="og:url" content="'; bloginfo('url'); echo '">';echo "\n";
echo '<meta property="og:type" content="website">';echo "\n";
}
$str = $post->post_content;
$searchPattern = '/<img.*?src=(["\'])(.+?)\1.*?>/i';
if (is_single()){
if (has_post_thumbnail()){
$image_id = get_post_thumbnail_id();
$image = wp_get_attachment_image_src( $image_id, 'full');
echo '<meta property="og:image" content="'.$image[0].'">';echo "\n";
} else if ( preg_match( $searchPattern, $str, $imgurl )){
echo '<meta property="og:image" content="'.$imgurl[2].'">';echo "\n";
} else {
echo '<meta property="og:image" content="http://genkimaru.club/cms/wp-content/themes/genkimaru/images/fb_ico.jpg">';echo "\n";
}
} else {
echo '<meta property="og:image" content="http://genkimaru.club/cms/wp-content/themes/genkimaru/images/fb_ico.jpg">';echo "\n";
}
?>
