<?php

//-------------------------------------------
// 使用しないメニューを非表示にする
//-------------------------------------------
function remove_admin_menus() {
 
    // level10以外のユーザーの場合
    if (!current_user_can('level_10')) {

        global $menu;

        // unsetで非表示にするメニューを指定
        unset($menu[2]);        // ダッシュボード
//        unset($menu[5]);        // 投稿
        unset($menu[10]);       // メディア
        unset($menu[20]);       // 固定ページ
        unset($menu[25]);       // コメント
        unset($menu[60]);       // 外観
        unset($menu[65]);       // プラグイン
       // unset($menu[70]);       // ユーザー
        unset($menu[75]);       // ツール
        unset($menu[80]);       // 設定
		// カスタムフィールド非表示
		function remove_acf_menu(){
			remove_menu_page('edit.php?post_type=acf');
		}
		add_action( 'admin_menu', 'remove_acf_menu', 999 );

		// SEO All in one非表示
		function custom_admin_menu() {
			remove_menu_page( 'all-in-one-seo-pack/aioseop_class.php' );
		}
		add_action( 'admin_menu', 'custom_admin_menu', 1000 );
    }
}
add_action('admin_menu', 'remove_admin_menus');

//-------------------------------------------
//管理画面の「見出し１」等を削除する
//-------------------------------------------

function custom_editor_settings( $initArray ){
$initArray['block_formats'] = "段落=p; 見出し1=h3; 見出し2=h4;";
return $initArray;
}
add_filter( 'tiny_mce_before_init', 'custom_editor_settings' );

//-------------------------------------------
//管理画面の「見出し１」等を削除する
//-------------------------------------------
function TinyMceInitOptions4BrOnly( $initArray )
{
	$initArray = array_merge( $initArray,  array('force_br_newlines' =>  true) );
	$initArray = array_merge( $initArray,  array('forced_root_block' => '') );
	$initArray = array_merge( $initArray,  array('force_p_newlines' => false) );
	return $initArray;
}

add_filter('teeny_mce_before_init', 'TinyMceInitOptions4BrOnly');
add_filter('tiny_mce_before_init', 'TinyMceInitOptions4BrOnly');
//-------------------------------------------
//投稿のカテゴリーとタグを非表示
//-------------------------------------------

function remove_menu() {
//  if (current_user_can('editor')) {
    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
 //  }
}
add_action('admin_menu', 'remove_menu');
//-------------------------------------------
//投稿部分表示カスタマイズ
//-------------------------------------------
function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'スタッフブログ';
	$submenu['edit.php'][5][0] = 'スタッフブログ一覧';
	$submenu['edit.php'][10][0] = '新規作成';
	$submenu['edit.php'][16][0] = 'タグ';
	//echo ”;
}
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'スタッフブログ';
	$labels->singular_name = 'スタッフブログ';
	$labels->add_new = _x('新規作成', '新着情報');
	$labels->add_new_item = '新しい情報';
	$labels->edit_item = '情報の編集';
	$labels->new_item = '新しい情報';
	$labels->view_item = '新着情報を表示';
	$labels->search_items = '新着情報検索';
	$labels->not_found = '新着情報が見つかりませんでした';
	$labels->not_found_in_trash = 'ゴミ箱の新着情報にも見つかりませんでした';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );



/* 記事内のカテゴリー追加を非表示 */
function hide_category_tabs_adder() {
	global $pagenow;
	global $post_type;
	if (is_admin() && ($pagenow=='post-new.php' || $pagenow=='post.php')){
		echo '<style type="text/css">
		.taxonomy-add-new {display:none;}
		</style>';
	}
}
add_action( 'admin_head', 'hide_category_tabs_adder' );


//-------------------------------------------
// スマホならtrue, タブレット・PCならfalseを返す
//-------------------------------------------

global $is_mobile;
$is_mobile = false;

$useragents = array(
 'iPhone',          // iPhone
 'iPod',            // iPod touch
 'Android',         // 1.5+ Android
 'dream',           // Pre 1.5 Android
 'CUPCAKE',         // 1.5+ Android
 'blackberry9500',  // Storm
 'blackberry9530',  // Storm
 'blackberry9520',  // Storm v2
 'blackberry9550',  // Storm v2
 'blackberry9800',  // Torch
 'webOS',           // Palm Pre Experimental
 'incognito',       // Other iPhone browser
 'webmate'          // Other iPhone browser
 );
 $pattern = '/'.implode('|', $useragents).'/i';
 if( preg_match($pattern, $_SERVER['HTTP_USER_AGENT'])){
	$is_mobile = preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
 }

function is_mobile(){
	global $is_mobile;
	return $is_mobile;
	//return false;
}
function is_pc(){
	if(is_mobile()){
		return false;
	}else{
		return true;
	}
}
function mobile_img(){
	if (is_mobile()) {
		echo "_sp";
	}
}


//-------------------------------------------
//プレビューボタン非表示
//-------------------------------------------
add_action('admin_print_styles', 'admin_preview_css_custom');
function admin_preview_css_custom() {
   echo '<style>#preview-action {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
}

//-------------------------------------------
//記事画像パスを相対パスで利用する
//-------------------------------------------

function delete_host_from_attachment_url( $url ) {
    $regex = '/^http(s)?:\/\/[^\/\s]+(.*)$/';
    if ( preg_match( $regex, $url, $m ) ) {
        $url = $m[2];
    }
    return $url;
}
add_filter( 'wp_get_attachment_url', 'delete_host_from_attachment_url' );
add_filter( 'attachment_link', 'delete_host_from_attachment_url' );

function replaceImagePath($arg) {
	$content = str_replace('"images/', '"' . get_bloginfo('template_directory') . '/images/', $arg);
	return $content;
}  
add_action('the_content', 'replaceImagePath');
register_sidebar();



//-------------------------------------------
//ページャー
//-------------------------------------------
function bmPageNaviGallery() {
  global $wp_rewrite;
  global $wp_query;
  global $paged;
 
  $paginate_base = get_pagenum_link(1);
  if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
    $paginate_format = '';
    $paginate_base = add_query_arg('page', '%#%');
  } else {
    $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '') .
    untrailingslashit('page/%#%', 'paged');
    $paginate_base .= '%_%';  
  }
  $cat = "";
  if(!empty($_GET['cat'])){
	$cat = "&cat=".htmlspecialchars($_GET['cat'], ENT_QUOTES);
  }
  //$paginate_base = "./page/%#%".$cat;
  $result = paginate_links( array(
   // 'base' => $paginate_base,
    'format' => $paginate_format,
    'total' => $wp_query->max_num_pages,
    'mid_size' => 4,
    'prev_text' => '&lt;&lt;',
    'next_text' => '&gt;&gt;',
    'current' => ($paged ? $paged : 1),
  ));
 
  return $result;
}

add_action( 'admin_print_footer_scripts', 'limit_category_select' );
function limit_category_select() {
	?><script type="text/javascript">
		jQuery(function($) {
			// 投稿画面のカテゴリー選択を制限
			var cat_checklist = $('.categorychecklist input[type=checkbox]');
			cat_checklist.click( function() {
				$(this).parents('.categorychecklist').find('input[type=checkbox]').attr('checked', false);
				$(this).attr('checked', true);
			});
			
			// クイック編集のカテゴリー選択を制限
			var quickedit_cat_checklist = $('.cat-checklist input[type=checkbox]');
			quickedit_cat_checklist.click( function() {
				$(this).parents('.cat-checklist').find('input[type=checkbox]').attr('checked', false);
				$(this).attr('checked', true);
			});
		    
			$('.categorychecklist>li:first-child, .cat-checklist>li:first-child').before('<p style="padding-top:5px;">カテゴリーは1つしか選択できません</p>');
		});
	</script><?php
}



function remove_post_metaboxes() {
remove_meta_box('categorydiv', 'post', 'normal');

}
add_action('admin_menu', 'remove_post_metaboxes');

function remove_post_metaboxes2() {
remove_meta_box('tagsdiv-post_tag', 'post', 'normal');
}
add_action('admin_menu', 'remove_post_metaboxes2');

?>