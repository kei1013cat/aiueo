// JavaScript Document
  function attachMessage(marker, msg) {
    google.maps.event.addListener(marker, 'click', function(event) {
      new google.maps.InfoWindow({
        content: msg
      }).open(marker.getMap(), marker);
    });
  }
// 位置情報と表示データの組み合わせ
  var data = new Array();//マーカー位置の緯度経度
  data.push({position: new google.maps.LatLng(43.048259, 141.448070), content: 'トーマス'});
 
  var myMap = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,//地図縮尺
    center: new google.maps.LatLng(43.048259, 141.448070),//地図の中心点
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });


  for (i = 0; i < data.length; i++) {
    var myMarker = new google.maps.Marker({
      position: data[i].position,
      map: myMap
    });
    attachMessage(myMarker, data[i].content);
  }
