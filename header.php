<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php if(is_pc()): ?>
    <meta content="width=1040" name="viewport">
    <?php else: ?>
    <meta name="viewport" content="width=device-width" />
    <?php endif; ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title>
        <?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo ' | '; } ?>
        <?php bloginfo('name'); ?>
    </title>
    <meta name="keywords" content="あいうえお,札幌市,東区,児童発達支援,放課後等デイサービス,児童デイサービス" />
    <?php wp_head(); ?>
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" />
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <?php if(is_mobile()){ ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/sp.css" type="text/css">
    <?php }else{ ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/pc.css" type="text/css">
    <?php } ?>

    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.11.1.min.js"></script>

    <?php if(is_mobile()){ ?>
    <script src="<?php bloginfo('template_url'); ?>/js/iscroll.min.js"></script>
    <!-- drawer -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/drawer/dist/css/drawer.min.css">
    <script src="<?php bloginfo('template_url'); ?>/js/drawer/dist/js/drawer.min.js"></script>
    <script type="text/javascript">
        jQuery(function($) {
            $('.drawer').drawer();
            // ドロワーメニューが開いたとき
            $('.drawer').on('drawer.opened', function() {
                $('#menu-text').text('CLOSE');
            });
            // ドロワーメニューが閉じたとき
            $('.drawer').on('drawer.closed', function() {
                $('#menu-text').text('MENU');
            });
        });

    </script>
    <?php }else{ ?>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/header_parallax"></script>
    <?php } ?>

    <script>
        jQuery(function() {
            $('#loading_wrap').delay(100).fadeOut("slow");
        });

    </script>

    <!-- scrollreveal -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.thema.js"></script>

    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/top.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/form/mfp.statics/mailformpro.css" type="text/css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/lightbox.css" type="text/css">


    <?php if ( is_home() ) {?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slider/dist/css/slider-pro.min.css" />
    <script src="<?php bloginfo('template_url'); ?>/js/slider/dist/js/jquery.sliderPro.min.js"></script>
    <script>
        $(document).ready(function() {
            $('body').fadeIn(500);
            $('#slide_text').delay(100).fadeIn(2000).delay(25000).fadeOut(1000);
            $('#slider1').sliderPro({
                width: "100%",
                <?php if(is_pc()): ?> height: 570,
                <?php endif; ?>
                <?php if(is_mobile()): ?> height: 600,
                <?php endif; ?>
                arrows: false, //矢印の有無
                buttons: false, //ページャーの有無
                autoplay: true, //自動スライドか否か
                fade: true,
                autoplayOnHover: 'none',
                autoplayDelay: 5000,
                fadeDuration: 1500,
                loop: true,
                touchSwipe: false,
                visibleSize: '100%',
                forceSize: 'fullWidth'
            });
        });

    </script>
    <?php } ?>

    <!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
<![endif]-->

</head>
<?php
    $body_id = "";
    if ( is_home() ) {
        $body_id = ' id="page_index"';
    }else if ( is_page() ) {
        $body_id = ' id="page_'.get_post($post->post_parent)->post_name.'" class="subpage drawer drawer--top"';
    }else if ( is_single() ) {
        $body_id = ' id="page_single" class="subpage drawer drawer--top"';
    }else if ( is_archive() ) {
        $body_id = ' id="page_archive" class="subpage drawer drawer--top"';
    }else if ( is_404() ) {
        $body_id = ' id="page_404" class="subpage drawer drawer--top"';
    }
?>
<body<?php echo $body_id; ?> class="drawer drawer--top">
    <div id="loading_wrap"></div>
    <div id="outer">
        <header class="global" id="pagetop">
            <?php if(is_pc()):?>
            <div class="top">
                <div class="cf wrapper">
                    <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.jpg" alt="あいうえお児童発達支援・放課後等デイサービス" /></a></h1>
                    <div class="center">
                        <!--                 <ul class="cf">
                    <li class="privacy"><a href="<?php bloginfo('url'); ?>/privacy/">プライバシーポリシー</a></li>
                    <li class="link"><a href="<?php bloginfo('url'); ?>/link/">リンク</a></li>
                    <li class="sitemap"><a href="<?php bloginfo('url'); ?>/site/">サイトマップ</a></li>
                </ul> -->
                        <p><a href="<?php bloginfo('url'); ?>/staffblog/"><img src="<?php bloginfo('template_url'); ?>/images/header_blog.jpg" alt="あいうえおスタッフブログ" class="alpha85"></a></p>
                    </div>
                    <!-- center -->
                    <div class="right cf">
                        <address>
                            〒065-0042　札幌市東区本町2条2丁目5番32号
                        </address>
                        <p class="tel"><img src="<?php bloginfo('template_url'); ?>/images/header_tel.jpg" alt="TEL:011-792-1182"></p>
                        <p class="time cf"><span class="h">受付</span><span class="t">9:00~18:00</span><br><span class="t2">(月～土)</span></p>
                    </div>
                    <!-- right -->
                </div>
                <!-- wrapper -->
            </div>
            <!-- top -->
            <div class="line"></div>

            <nav>
                <div class="wrapper">
                    <ul class="cf">
                        <li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/about/">施設のご案内</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/flow/">ご利用の流れ</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/faq/">よくあるご質問</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/introduction/">事業案内</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/company/">会社概要</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>
                    </ul>
                </div>
                <!-- wrapper -->
            </nav>
            <?php endif;?>
            <?php if(is_mobile()):?>
            <div class="spmenu drawermenu" role="banner" id="top">
                <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.jpg" /></a></h1>
                <button type="button" class="drawer-toggle drawer-hamburger">
                    <span class="sr-only">toggle navigation</span>
                    <span class="drawer-hamburger-icon"></span>
                    <div id="menu-text" class="text">MENU</div>
                </button>
                <nav class="drawer-nav" role="navigation">
                    <div class="nav-top">
                        <ul class="cf">
                            <li><a target="_blank" href="tel:0117921182"><img src="<?php bloginfo('template_url'); ?>/images/header_tel_icon.svg" /><span class="text">電話をかける</span></a></li>
                            <li><a target="_blank" href="https://goo.gl/maps/qAYGPU832vy"><img src="<?php bloginfo('template_url'); ?>/images/header_map_icon.svg" /><span class="text">Google Maps</span></a></li>
                        </ul>
                    </div>

                    <div class="inner">
                        <h3 class="sp-title">MENU</h3>
                        <ul class="drawer-menu cf">
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/">ホーム</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/about/">施設のご案内</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/flow/">ご利用の流れ</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/faq/">よくあるご質問</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/introduction/">事業案内</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/company/">会社概要</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/staffblog/">スタッフブログ</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>

                        </ul>
                    </div>
                    <!-- inner -->
                </nav>
            </div>
            <!-- spmenu -->
            <?php endif;?>
        </header>
        <main role="main">
