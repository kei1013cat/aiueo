<section class="pd">
    <h3 class="headline01 enter-top">ご利用の流れ</h3>

    <div class="inner40 enter-bottom">
        <div class="flow_bg">
            <h4>１.ご相談・ご見学</h4>
            <p class="t1 fead1">まずは、お気軽にお電話ください。<br>
                ご相談・ご見学はいつでも可能です。</p>
            <h4>２.モニタリング</h4>
            <p class="t2 fead2">現在の状況、ご利用に際しての<br>
                ご希望などをお伺いいたします。</p>
            <h4>３.ご契約</h4>
            <p class="t3 fead3">ご利用に当たってのご説明や、<br>
                保護者様の疑問等にお答えいたします。</p>
            <h4>４.ご利用計画の立案</h4>
            <p class="t4 fead4">アセスメントにて適切な支援内容を検討し、<br>
                提供するデイサービス計画の原案を作成し交付します</p>
            <h4>５.ご利用開始</h4>
            <p class="t5 fead5">デイサービス計画に沿ったご利用の<br>
                開始になります</p>
        </div>
    </div>
    <!-- inner40 -->

</section>
<section class="pd bg_beige">
    <div class="wrapper_inner">
        <h3 class="headline01 enter-top">ご利用時間</h3>
        <p class="enter-bottom">以下から、ご利用時間をお選びいただけます</p>
        <table class="style2 enter-bottom">
            <tr>
                <th scope="col">利用日</th>
                <th scope="col">利用時間</th>
                <th scope="col">利用対象</th>
            </tr>
            <tr>
                <td>①月曜日～土曜日</td>
                <td>9：00～13：00</td>
                <td>児童発達支援・<br>
                    放課後等デイサービス</td>
            </tr>
            <tr>
                <td>②月曜日～土曜日</td>
                <td>13：00～17：00</td>
                <td>児童発達支援・<br>
                    放課後等デイサービス</td>
            </tr>
            <tr>
                <td>③月曜日～土曜日</td>
                <td>9：00～17：00</td>
                <td>児童発達支援・<br>
                    放課後等デイサービス</td>
            </tr>
        </table>
        <p class="enter-bottom">①②③のご利用合計人数が、1日10名までになります。</p>
    </div><!-- wrapper_inner -->
</section>
<section class="pd">
    <div class="wrapper_inner">
        <h3 class="headline02 enter-top">保健福祉課窓口</h3>
        <table border="0" class="style2 enter-bottom">
            <tr>
                <th scope="col">名称</th>
                <th scope="col">住所</th>
                <th scope="col">電話番号</th>
            </tr>
            <tr>
                <td>札幌市 北区　保健福祉部保健福祉課</td>
                <td>〒001-0026　<br>
                    札幌市北区北24条西6丁目</td>
                <td>011-757-2465</td>
            </tr>
            <tr>
                <td>札幌市 東区　保健福祉部保健福祉課</td>
                <td>〒065-0011　<br>
                    札幌市東区北11条東7丁目</td>
                <td>011-741-2459</td>
            </tr>

            <tr>
                <td>札幌市 西区　保健福祉部保健福祉課</td>
                <td>〒063-8612　<br>
                    札幌市西区琴似2条7丁目1-1</td>
                <td>011-641-6942</td>
            </tr>
            <tr>
                <td>札幌市 白石区　保健福祉部保健福祉課</td>
                <td>〒003-8612　<br>
                    札幌市白石区南郷通1丁目南8</td>
                <td>011-861-2451</td>
            </tr>
            <tr>
                <td>札幌市 中央区　保健福祉課　相談窓口</td>
                <td>〒060-8612　<br>
                    札幌市中央区南3条西11丁目</td>
                <td>011-205-3306</td>
            </tr>
            
            <tr>
                <td>石狩市 保健福祉部障がい支援担当</td>
                <td>〒061-3216<br>
                    石狩市花川北6条1丁目<br>
                    41番地１　りんくる１階</td>
                <td>0133-72-3194</td>
            </tr>
            
        </table>
    </div><!-- wrapper_inner -->
</section>
