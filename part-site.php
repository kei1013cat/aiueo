<section class="pd">
    <div class="wrapper660">
        <h3 class="headline01 enter-top">サイトマップ</h3>

        <div class="cf enter-bottom">
            <ul class="left">
                <li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
                <li><a href="<?php bloginfo('url'); ?>/about/">施設のご案内</a></li>
                <li><a href="<?php bloginfo('url'); ?>/flow/">ご利用の流れ</a></li>
                <li><a href="<?php bloginfo('url'); ?>/faq/">よくあるご質問</a></li>
                <li><a href="<?php bloginfo('url'); ?>/introduction/">事業案内</a></li>
            </ul>
            <ul class="right">
                <li><a href="<?php bloginfo('url'); ?>/company/">会社概要</a></li>
                <li><a href="<?php bloginfo('url'); ?>/privacy/">プライバシーポリシー</a></li>
                <li><a href="<?php bloginfo('url'); ?>/link/">リンク</a></li>
                <li><a href="<?php bloginfo('url'); ?>/site/">サイトマップ</a></li>
                <li><a href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>
            </ul>
        </div><!-- cf -->


    </div>
    <!-- wrapper660 -->
</section>
