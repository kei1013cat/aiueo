<?php
/*
Template Name: page-staffblog
*/
?>
<?php get_header(); ?>

<div id="contents">
	<?php include (TEMPLATEPATH . '/part-title.php'); ?>
	<section class="news_list">
						<h3 class="headline01">スタッフブログ</h3>
            <div class="h_line"></div>
		<div class="wrapper660 cf">
			<div class="left_contents">
			
				<?php
				$paged = (int) get_query_var('paged');
		$wp_query = new WP_Query();
		$param = array(
			'post_status' => 'publish',
			'orderby' => 'date',
			'paged' => $paged,
			'order' => 'DESC'
		);
		$wp_query->query($param);?>
				<?php if($wp_query->have_posts()):?>
				<section>
				<div class="lay_postlist2">
				<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
		<dl class="cf">
            <dt><?php the_time('Y.m.d'); ?></dt>
            <dd><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php if(mb_strlen($post->post_title)>60) { $title= mb_substr($post->post_title,0,60) ; echo $title. '...' ;
} else {echo $post->post_title;}?></a></dd></dl>
				<?php endwhile; ?>
				
				
				</section>
				</div>
				<div class="pagination">
					<?php echo bmPageNaviGallery(); // ページネーション出力 ?>
				</div>
				<!-- pagination -->
				<?php else : ?>
				<p class="tac">記事が見つかりませんでした</p>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
			</div>
			<!-- left_cont -->
			

		</div>
		<!-- wrapper -->
	</section>
</div>
<!-- contents -->

<?php get_footer(); ?>
